<?php

class Titanium_Posts {
	private $master = null;
	
	public function __construct(Titanium $master) {
		$this->master = $master;
	}
	
	/**
	 * Deletes Posts objects that match the query constraints provided in the where parameter.
	 * If no where parameter is provided, all Posts objects are deleted.
	 * @param array $param
	 */
	public function batch_delete(array $param) {
		//@TODO
	}
	
	/**
	 * Retrieves the total number of Post objects.
	 * @return mixed
	 */
	public function count() {
		$params = array();
		return $this->master->call('posts/count', $params, false);
	}
	
	/**
	 * Create a post, which can be a Facebook-style wall post or Digg-style submission with content.
	 * @param array $params
	 * @return mixed
	 */
	public function create(array $params) {
		return $this->master->call('posts/create', $params);
	}
	
	/**
	 * Deletes the post with the given id. The original submitter can always delete a post.
	 */
	public function delete() {
		//@TODO
	}
	
	/**
	 * Performs custom query of posts with sorting and pagination.
	 * Currently you can not query or sort data stored inside array or hash in custom fields.
	 * @param array $params
	 * @return mixed
	 */
	public function query(array $params=array()) {
		return $this->master->call('posts/query', $params, false);
	}
	
	/**
	 * Returns the post with the given id.
	 * @param array $params
	 * @return mixed
	 */
	public function show(array $params) {
		return $this->master->call('posts/show', $params, false);
	}
	
	/**
	 * Updates the identified post. The original submitter can always update a post.
	 * @param array $params
	 */
	public function update(array $params) {
		//@TODO 
	}
}