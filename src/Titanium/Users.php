<?php

class Titanium_Users {
	private $master = null;
	
    public function __construct(Titanium $master) {
        $this->master = $master;
    }
    
    /**
     * Log a user in using their ArrowDB credentials. The first parameter can
     * be an array to keep all methods with similar 
     * @param string $login
     * @param string $password
     * @param boolean $pretty_json
     */
    public function login($login, $password = '', $pretty_json = false) {
    	if(is_array($login)) {
    		$_params = $login;
    	}else{
    		$_params = array(
    			'login' => $login,
    			'password' => $password,
    			'pretty_json' => $pretty_json
    		);
    	}
    	return $this->master->call('users/login', $_params);
    }

	/**
	 * Deletes Users objects that match the query constraints provided in the
	 * where parameter. If no where parameter is provided, all Users objects
	 * are deleted. Note that an HTTP 200 code (success) is returned if the
	 * call completed successfully but the query matched no objects.
	 * @TODO nao ta na hora
	 * @param string $where
	 */
    public function batch_delete($where){
    	
    }
    
    /**
     * Total number of Chat objects. Returned in the meta header.
     * @return mixed
     */
    public function count() {
    	$_params = array();
    	return $this->master->call('users/count', $_params, false);
    }
    
    /**
     * Log out a user.
     * 
     * @param string $device_token
     * @param string $pretty_json
     * @return mixed
     */
    public function logout($device_token = '', $pretty_json = false) {
    	$_params = array(
    			'device_token' => $device_token,
    			'pretty_json' => $pretty_json
    	);
    	return $this->master->call('users/logout', $_params, false);
    }
    
    /**
     * Custom query of Users objects with sorting and paginating. You can
     * query on sort based on the data in any of the standard User fields.
     * You can also query and sort data based on the values of any custom
     * fields, if the values are simple JSON values.
     * @param array $params
     * @return mixed
     */
    public function query(array $params = array()){
    	return $this->master->call('users/query', $params, false);
    }
    
    /**
     * Shows both public and private user information about the user who is currently logged in.
     * @param array $params
     * @return mixed
     */
    public function show_me(array $params=array()){
    	return $this->master->call('users/show/me', $params, false);
    }
    
    /**
     * You can use this API to re-send user verification email.
     */
    public function resend_confirmation($params){
    	$_params = $params;
    	return $this->master->call('users/resend_confirmation', $_params, false);
    }
    public function create(){}
    public function delete(){}
    public function request_reset_password(){}
    public function show(){}
    public function update(){}
    
}


