<?php

class Titanium_Chats {
	private $master = null;
	
	public function __construct(Titanium $master) {
		$this->master = $master;
	}
	/**
	 * Total number of Chat objects. Returned in the meta header.
	 */
	public function count(){
		$_params = array();
		return $this->master->call('chats/count', $_params, false);
	}
	/**
	 * Sends a chat message to another user or a group of users.Sending a
	 * message creates a new chat group if there is no existing chat group
	 * containing the current user and the designated recipients.To generate
	 * a push notification, include the channel and payload parameters in the
	 * array.
	 */
	public function create(array $params){
		return $this->master->call('chats/create', $params);
	}
	/**
	 * Deletes a chat message.
	 */
	public function delete(){}
	/**
	 * Lists chat groups.If user A sends chat message to user B and C, users A,
	 * B and C automatically form a chat group. Use this API to get a list of
	 * chat groups the current user belongs to.
	 */
	public function get_chat_groups(array $params = array()){
		return $this->master->call('chats/get_chat_groups',$params,$false);
	}
	/**
	 * Performs a custom query of chat messages with sorting and pagination.
	 * Currently you can not query or sort data stored inside array or hash in
	 * custom fields.
	 */
	public function query(array $params = array()){
		return $this->master->call('chats/query', $params, false);
	}
}