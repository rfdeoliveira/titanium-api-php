# TITANIUM-API-PHP #

## Example ##

```php
session_start(); //the cookies are kept in session variable
require_once 'src/Titanium.php'; //or use composer
$ti = new Titanium('your-appcelerator-api-key');

$result = $ti->users->login('username','password');
var_dump($result);
```


## This project is inspired by mandrill-api-php ##
https://bitbucket.org/mailchimp/mandrill-api-php
Thanks for giving us a direction!